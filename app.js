const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const Mongoose = require('mongoose')
Mongoose.Promise = global.Promise
const apiUfa = require('./app/service/ufa/routes/router')
const cors = require('cors')
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
require('dotenv').config()

const NODE_ENV = process.env.NODE_ENV || 'development'
// Mongoose
if (NODE_ENV !== 'test') {
  const mongodbHost = process.env.MONGO_URL || '128.199.147.162:27017/ufa'
  const mongoUsername = process.env.MONGO_USERNAME || 'adminufa'
  const mongoPassword = process.env.MONGO_PASSWORD || 'aabb1234'
  const mongoUrl =
mongoUsername !== '' && mongoPassword !== ''
  ? `mongodb://${mongoUsername}:${mongoPassword}@${mongodbHost}`
  : `mongodb://${mongodbHost}`
  console.log(mongoUrl)
  const opts = { useNewUrlParser: true, useUnifiedTopology: true }
  Mongoose.connect(mongoUrl, opts).then(() => {
    const server = app.listen(7040, function () {
      const host = server.address().address
      const port = server.address().port
      console.log('Application Run AT htttp://%s:%s', host, port)
    })
  }).catch((err) => {
    console.log(err)
    process.exit(1)
  })
  Mongoose.set('useFindAndModify', false)
  Mongoose.set('useCreateIndex', true)
}
// route truewallet//
app.use('/api/ufa', apiUfa)
