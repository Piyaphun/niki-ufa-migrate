const HTTPStatus = require('http-status')

const { ufaSignup, ufaGetCredit, ufaTransfer, ufaLogin } = require('../lib/ufa')

async function login (req, resSign) {
  ufaLogin(req, (err, data) => {
    console.log(err, data)
    if (!err) {
      resSign.status(HTTPStatus.OK).json(data)
    } else {
      resSign.status(HTTPStatus.BAD_REQUEST).json({ status: err })
    }
  })
}
async function signup (req, resSign) {
  ufaSignup(req, (err, data) => {
    console.log(err, data)
    if (!err) {
      resSign.status(HTTPStatus.CREATED).json({ status: data })
    } else {
      resSign.status(HTTPStatus.BAD_REQUEST).json({ status: err })
    }
  })
}

async function getCredit (req, resCrdit) {
  ufaGetCredit(req, (err, data) => {
    if (!err) {
      resCrdit.status(HTTPStatus.OK).json({ Credit: data })
    } else {
      resCrdit.status(HTTPStatus.BAD_REQUEST).json({ status: err })
    }
  })
}

async function transfer (req, resTransfer) {
  ufaTransfer(req, (err, data) => {
    if (!err) {
      resTransfer.status(HTTPStatus.OK).json(data)
    } else {
      resTransfer.status(HTTPStatus.BAD_REQUEST).json({ status: err })
    }
  })
}

module.exports = { login, signup, getCredit, transfer }
