const Mongoose = require('mongoose')
const Schema = Mongoose.Schema

const AgentSchema = new Schema({
  usernameAgent: { type: String, required: true },
  passwordAgent: { type: String, required: true },
  cookieAuth: { type: String, required: true }
}, {
  timestamps: true
})

module.exports = Mongoose.model('agent', AgentSchema)
