let request = require('request')
const cheerio = require('cheerio')
const qs = require('querystring')
const Tesseract = require('tesseract.js')
const fs = require('fs')
const generator = require('generate-password')
const { prepareDataSingup, prepareDataUpdate } = require('../lib/prepare')
const { ufaChangePassPlayer } = require('../lib/ufaPlayer')
const jimp = require('jimp')
const j = request.jar()
const Agent = require('../models/agent')

request = request.defaults({
  jar: j
})
const baseUrlUfa = 'https://ag1.ufabet.com'
const namCode = generator.generate({
  length: 10,
  numbers: true
})
function ufaLogout () {
  request(`${baseUrlUfa}/signout.aspx`, (errLogout, resLogout, bodyLogout) => {
    if (errLogout) {
      console.log(errLogout)
    } else {
      console.log('Logout')
    }
  })
}
async function ufaLogin (req, callback) {
  const checkUsername = await Agent.findOne({
    usernameAgent: req.body.usernameAgent,
    passwordAgent: req.body.passwordAgent
  })
  if (checkUsername) {
    const cookies = checkUsername.cookieAuth.split('; ')
    if (cookies) cookies.forEach((c) => { j.setCookie(c, baseUrlUfa, { ignoreError: true }) })
    try {
      request(`${baseUrlUfa}/_Age1/MemberSet.aspx`, (err1, res, body) => {
        const $ = cheerio.load(body)
        const checkForm = $('#form1').attr('action')
        if (checkForm.includes('Default11.aspx')) {
          const errRes = 'Login Expire'
          console.log(errRes)
          ufaLoginNew(req, (err, data) => {
            if (err) {
              callback(err)
            } else {
              callback(null, 'success')
            }
          })
        } else {
          callback(null, 'success')
        }
      })
    } catch (err) {
      callback(err)
    }
  } else {
    ufaLoginNew(req, (err, data) => {
      if (err) {
        callback(err)
      } else {
        callback(null, 'success')
      }
    })
  }
}
async function ufaLoginNew (req, callback) {
  console.log(j)
  try {
    const loginUrl = `${baseUrlUfa}/Public/Default11.aspx`
    const usernameAgent = req.body.usernameAgent
    const passwordAgent = req.body.passwordAgent
    const header1 = {
      url: loginUrl,
      method: 'GET',
      jar: j
    }
    const messageError = 'login false'
    request.get(header1, (err, response, body) => {
      if (!err && response.statusCode === 200) {
        const pathLogin = response.request.path
        const $ = cheerio.load(body)
        const VIEWSTATE = $('#form1 > input#__VIEWSTATE').val()
        const VIEWSTATEGENERATOR = $('#form1 > input#__VIEWSTATEGENERATOR').val()
        const EVENTVALIDATION = $('#form1 > input#__EVENTVALIDATION').val()
        const txtUserName = usernameAgent
        const txtPassword = passwordAgent
        const headersData = {
          uri: `${baseUrlUfa}/Public/img.aspx?r=21290147`,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          jar: j
        }
        request.head(headersData, async (err, response, body) => {
          if (!err && response.statusCode === 200) {
            request(headersData).pipe(fs.createWriteStream(`textCode/${namCode}.gif`)).on('close', async function () {
              // gifFrames({ url: `textCode/${namCode}.gif`, frames: 0 }).then(function (frameData) {
              //   frameData[0].getImage().pipe(fs.createWriteStream(`textCode/${namCode}.jpg`))
              // })

              const image = await jimp.read(`textCode/${namCode}.gif`)
              image.quality(60).greyscale().write(`textCode/${namCode}.jpg`)
              // const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs))
              // await sleep(50)

              Tesseract.recognize(
                `textCode/${namCode}.jpg`,
                'eng',
                {
                  init_oem: Tesseract.OEM.TESSERACT_ONLY,
                  tessedit_char_whitelist: '0123456789'
                }
              ).then(({ data: { text } }) => {
                fs.unlinkSync(`textCode/${namCode}.jpg`)
                fs.unlinkSync(`textCode/${namCode}.gif`)
                const txtCode = text.replace('\n', '')
                const dataLogin = {
                  __EVENTTARGET: 'btnSignIn',
                  __EVENTARGUMENT: '',
                  __VIEWSTATE: VIEWSTATE,
                  __VIEWSTATEGENERATOR: VIEWSTATEGENERATOR,
                  __EVENTVALIDATION: EVENTVALIDATION,
                  hid_rd: '1',
                  txtUserName: txtUserName,
                  txtPassword: txtPassword,
                  txtCode: txtCode,
                  lstLang: pathLogin
                }

                const headersData = {
                  uri: loginUrl,
                  form: qs.stringify(dataLogin),
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                  },
                  jar: j
                }

                request.post(headersData, async (err, res, body) => {
                  if (err) {
                    callback(err)
                  } else {
                    const $ = cheerio.load(body)
                    const checkLogin = $('a').attr('href')
                    if (checkLogin.includes('Main.aspx')) {
                      callback(null, 'success')
                      console.log('login success')
                      const checkUsername = await Agent.findOne({
                        usernameAgent: req.body.usernameAgent,
                        passwordAgent: req.body.passwordAgent
                      })
                      if (checkUsername) {
                        const updateAgent = await Agent.findOneAndUpdate({
                          usernameAgent: req.body.usernameAgent,
                          passwordAgent: req.body.passwordAgent
                        }, { cookieAuth: j.getCookieString(baseUrlUfa) })
                        console.log('update agent', updateAgent)
                      } else {
                        const createAgent = await Agent.create({
                          usernameAgent: req.body.usernameAgent,
                          passwordAgent: req.body.passwordAgent,
                          cookieAuth: j.getCookieString(baseUrlUfa)
                        })
                        console.log('update agent', createAgent)
                      }
                      const cookies = j.getCookieString(baseUrlUfa).split('; ')
                      if (cookies) cookies.forEach((c) => { j.setCookie(c, baseUrlUfa, { ignoreError: true }) })
                    } else {
                      console.log(messageError)
                      callback(messageError)
                    }
                  }
                })
              })
            })
          }
        })
      } else {
        callback(err)
      }
    })
  } catch (err) {
    callback(err)
  }
}

function ufaSignup (req, callback) {
  ufaLogin(req, (err, data) => {
    if (!err) {
      const usernameAgent = req.body.usernameAgent
      const usernameUser = req.body.usernameUser
      const shortname = usernameUser.replace(usernameAgent, '')
      const userNamePlayer = `${usernameAgent}${shortname}`
      const passwordPlayer = 'aabb1234'
      const passwordPlayerGen = generator.generate({
        length: 10,
        numbers: true
      })

      const reqPlayer = {
        usernamePlayer: userNamePlayer,
        passwordPlayer: passwordPlayer,
        passwordPlayerGen: passwordPlayerGen
      }
      try {
        request(`${baseUrlUfa}/_Age1/MemberSet.aspx`, (err1, res1, body1) => {
          const $ = cheerio.load(body1)
          const SignupVIEWSTATE = $('#form1 > input#__VIEWSTATE').val()
          const SignupVIEWSTATEGENERATOR = $('#form1 > input#__VIEWSTATEGENERATOR').val()
          const SignupEVENTVALIDATION = $('#form1 > input#__EVENTVALIDATION').val()
          const dataSignupPlayer = prepareDataSingup(SignupVIEWSTATE, SignupVIEWSTATEGENERATOR, SignupEVENTVALIDATION, shortname, passwordPlayer)
          const headersDataSignup = {
            uri: `${baseUrlUfa}/_Age1/MemberSet.aspx`,
            form: qs.stringify(dataSignupPlayer),
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          }
          request.post(headersDataSignup, (errSigup, resSigup, bodySigup) => {
            if (errSigup) {
              callback(errSigup)
            } else {
              const $ = cheerio.load(bodySigup)
              const status = $('#lblStatus > span').text()
              if (status === 'Profile updated successfully.') {
                ufaChangePassPlayer(reqPlayer, (err, data) => {
                  if (err) {
                    callback(err)
                  } else {
                    callback(null, { status: status, data: { username: userNamePlayer, password: passwordPlayerGen } })
                  }
                })
              } else {
                callback(status)
              }
            }
          })
        })
      } catch (err) {
        callback(err)
      }
    } else {
      callback(err)
    }
  })
}

function ufaGetCredit (req, callback) {
  ufaLogin(req, (err, data) => {
    if (!err) {
      try {
        const usernameAgent = req.body.usernameAgent
        const usernameUser = req.body.usernameUser
        request(`${baseUrlUfa}/_Age/MemberList.aspx?type=member&role=ag&userName=${usernameAgent}&sKey=${usernameUser}&isDeleted=-1&sortByType=0`, (err2, res2, body2) => {
          const $ = cheerio.load(body2)
          const credit = $('#MemberList_cm1_g > tbody > tr.GridItem > td:nth-child(7) > span').text().split(',').join('')
          if (credit) {
            console.log('credit', credit)
            callback(null, credit)
            //
          } else {
            const error = 'something wrong...'
            callback(error)
          }
        })
      } catch (err) {
        callback(err)
      //
      }
    } else {
      callback(err)
    }
  })
}

function ufaTransfer (req, callback) {
  ufaLogin(req, (err, data) => {
    const errorMessage = 'something wrong...'
    if (!err) {
      try {
        const usernameAgent = req.body.usernameAgent
        const amount = req.body.amount
        const usernameUser = req.body.usernameUser
        const shortname = usernameUser.replace(usernameAgent, '')
        request(`${baseUrlUfa}/_Age/MemberList.aspx?type=member&role=ag&userName=${usernameAgent}&sKey=${shortname}&isDeleted=-1&sortByType=0`, (err2, res2, body2) => {
          const $ = cheerio.load(body2)
          const credit = $('#MemberList_cm1_g > tbody > tr.GridItem > td:nth-child(7) > span').text().replace(',', '')
          console.log(credit)
          if (credit) {
            const creditIsTrue = Number(credit) + Number(amount)
            console.log(creditIsTrue)
            request(`${baseUrlUfa}/_Age1/MemberSet.aspx?userName=${usernameUser}&set=1`, (err2, res2, body2) => {
              const $ = cheerio.load(body2)

              const SignupVIEWSTATE = $('#form1 > input#__VIEWSTATE').val()

              const SignupVIEWSTATEGENERATOR = $('#form1 > input#__VIEWSTATEGENERATOR').val()

              const SignupEVENTVALIDATION = $('#form1 > input#__EVENTVALIDATION').val()
              const dataUpdatePlayer = prepareDataUpdate(SignupVIEWSTATE, SignupVIEWSTATEGENERATOR, SignupEVENTVALIDATION, creditIsTrue)

              const headersDataTransfer = {
                uri: `${baseUrlUfa}/_Age1/MemberSet.aspx?userName=${usernameUser}&set=1`,
                form: qs.stringify(dataUpdatePlayer),
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
                }
              }
              request.post(headersDataTransfer, (err, res, body) => {
                if (err) {
                  console.log(err)
                } else {
                  const $ = cheerio.load(body)
                  const status = $('#lblStatus > span').text()

                  if (status === 'Profile updated successfully.') {
                    console.log({ status: status, credit: creditIsTrue })
                    callback(null, { status: status, credit: creditIsTrue })
                  } else {
                    callback(errorMessage)
                  }
                }
              })
            })
          } else {
            callback(errorMessage)
          }
        })
      } catch (err) {
        callback(err)
      }
    } else {
      callback(err)
    }
  })
}

module.exports = { ufaLogin, ufaLoginNew, ufaSignup, ufaLogout, ufaGetCredit, ufaTransfer }
