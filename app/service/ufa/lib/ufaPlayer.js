let request = require('request')
const cheerio = require('cheerio')
const qs = require('querystring')
const j = request.jar()
request = request.defaults({
  jar: j
})

// Funtion เปลี่ยน pass ของ user
function ufaChangePassPlayer (req, callback) {
  try {
    const loginUrl = 'https://www.boss369.com/Default8.aspx?lang=EN-GB'
    const usernamePlayer = req.usernamePlayer
    const passwordPlayer = req.passwordPlayer
    const passwordPlayerGen = req.passwordPlayerGen
    const header1 = {
      url: loginUrl,
      method: 'GET',
      jar: j
    }

    request.get(header1, (err, response, body) => {
      if (!err && response.statusCode === 200) {
        const pathLogin = response.request.path
        const $ = cheerio.load(body)
        const VIEWSTATE = $('#form1 > input#__VIEWSTATE').val()
        const VIEWSTATEGENERATOR = $('#form1 > input#__VIEWSTATEGENERATOR').val()
        const txtUserName = usernamePlayer
        const password = passwordPlayer

        const dataLogin = {
          __EVENTTARGET: 'btnLogin',
          __EVENTARGUMENT: '',
          __VIEWSTATE: VIEWSTATE,
          __VIEWSTATEGENERATOR: VIEWSTATEGENERATOR,
          txtUserName: txtUserName,
          password: password,
          lstLang: pathLogin
        }

        // console.log(dataLogin)

        const headersData = {
          uri: loginUrl,
          form: qs.stringify(dataLogin),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          jar: j
        }
        request.post(headersData, (err, res, body) => {
          if (err) {
            callback(err)
          } else {
            const $ = cheerio.load(body)
            const checkLogin = $('#form1 > script').html()
            if (checkLogin === 'alert(\'Invalid user name or password!\');') {
              const message = 'Invalid user name or password!'
              callback(message)
            } else {
              const header1 = {
                url: 'https://www.boss369.com/Public/ChgPwd2.aspx?lang=EN-GB',
                method: 'GET',
                jar: j
              }

              request.get(header1, (err, response, body) => {
                if (!err && response.statusCode === 200) {
                  //   console.log(body)
                  const $ = cheerio.load(body)
                  const VIEWSTATE = $('#form1 > input#__VIEWSTATE').val()
                  const VIEWSTATEGENERATOR = $('#form1 > input#__VIEWSTATEGENERATOR').val()
                  const EVENTVALIDATION = $('#form1 > input#__EVENTVALIDATION').val()
                  const dataChangePass = {
                    __EVENTTARGET: 'btnSave',
                    __EVENTARGUMENT: '',
                    __VIEWSTATE: VIEWSTATE,
                    __VIEWSTATEGENERATOR: VIEWSTATEGENERATOR,
                    __EVENTVALIDATION: EVENTVALIDATION,
                    txtOldPassword: password,
                    txtNewPassword: passwordPlayerGen,
                    txtConfirmPassword: passwordPlayerGen,
                    btnSave: 'ยืนยัน'
                  }

                  const headersData = {
                    uri: 'https://www.boss369.com/Public/ChgPwd2.aspx?lang=EN-GB',
                    form: qs.stringify(dataChangePass),
                    headers: {
                      'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    jar: j
                  }
                  request.post(headersData, (err1, res1, body2) => {
                    callback(null, 'success')
                    // console.log(body2)
                  })
                } else {
                  callback(err)
                }
              })
            }
          }
        })
      } else {
        callback(err)
      }
    })
  } catch (err) {
    callback(err)
  }
}
module.exports = { ufaChangePassPlayer }
