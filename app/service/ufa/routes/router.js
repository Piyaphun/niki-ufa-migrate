const express = require('express')
const router = express.Router()
const ufa = require('../controllers/ufa')

router.route('/signup').post(ufa.signup)
router.route('/getCredit').post(ufa.getCredit)
router.route('/transfer').post(ufa.transfer)
router.route('/login').post(ufa.login)
module.exports = router
